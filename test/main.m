//
//  main.m
//  test
//
//  Created by Jinze YU on 2/19/16.
//  Copyright © 2016 Jinze YU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
