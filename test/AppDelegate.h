//
//  AppDelegate.h
//  test
//
//  Created by Jinze YU on 2/19/16.
//  Copyright © 2016 Jinze YU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

